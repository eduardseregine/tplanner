# TPlanner

Keep your tasks under control via `BotaloBot` Telegram channel using @GTD methodology. 

### Key functionality:

 - Categorise before saving and save entries persistently.
 - Change category if needed
 - Use comprehensive search engine
 - Mark `done` or `remove` an entry or multiple selections
 - Assign an absolute or relative date for a task and get this date a reminder the first time you start using TPlanner.

## Getting started

You can always get a link to this instruction by requesting `help`.
1. Intall Telegram application and find BotaloBot chat. 
2. Subscribe and press `Start`
3. You will be prompted to enter a pin, that you can receive by requesting eduardseregine@gmail.com.
4. Attention! 3 wrong attempts will block bot functionality.
5. You will be requested your name and the city, those you can change any time later on. Name is used to greet you every new day, and city - to update you on the weather forecast (once per day, functionality will be introduced later).
6. Once authorised the further authentification will be conducted automatically using your Telegram ID and will no longer require any efforts from your side.

## Basic Use of your TPlanner

### Save functionality
- Introduce more than 2 words whatever you want to have as a task.
- You will be prompted that the task is successfully saved: 
`Saved: Update me on TPlanner use (category: NoCAT)`

## Change category for the saved entry 
- If you don't like the category assigned automatically, introduce "change category {Your category}"
 - You will be promted that `Your data is successfully changed`.

### Search functionality
- Search is limited to the tasks that are not marked as 'Done'. To search in 'Done' - please refer to respective section below.
- Introduce up to 2 words that should be present either in the category or in the entry itself, e.g. "update". You will be prompted: 
```
Knowledge
1. Update me on TPlanner use
```
, where `Knowledge` is a name of the category assigned (or re-assigned, see Save functionality above).
-  Note, that the search first looks into the category and if any entry is found, no further search is performed.
- Second, it looks into the verb and nouns of your entries solely, and only after that do a full text search.
- If you introduced more than one word, the search produces a result only if both are present.
- In case of no entries that satisfy your search request are found, `Nothing to display` is produced.

## Change category for any found entry 
- If you don't like the category assigned to an entry in the list produced as a serach result, introduce "change category {NumberInTheList} {Your category}".
- For e.g., if you have been produced as the result of the search:
```
Knowledge
1. Update me on TPlanner use
```
, you can change 'Knowledge' to for e.g. 'TPlanner' by typing `change category 1 TPlanner`.
- You will be promted that `Your data is successfully changed`.
- You can re-perform search with `TPlanner` to ensure the category is changed.

### Search Category
 - To list all categories - enter `category`.
 - Thereafter you can type the name of the found category to see which entries it contains.

### Search Everything
- In order to see all your active (i.e. not marked as 'Done') tasks, introduce `*`.

### Search 'Done`
- In order to see all your done tasks, introduce `done`. You will get a list of tasks:
```
OnControl
1. Check if the TPlanner functionality is working properly (done on: 6 May)
```
- TPlanner will indicate the date when the task was marked 'Done' by yourself.

### Mark 'Done' functionality
- In order to mark any task as 'Done' - you need first to find it using any search criteria, e.g. Category (please refer to the respective section above).
- The search will produce the result that is a some numbered list of your entries (Note, numbering can be different based on your search criteria).
- For e.g. search has made the following produce:
```
Knowledge
1. Update me on TPlanner use
```
- In order, to mark this task as 'Done' introduce `+1`, where '+' stands for "Done" and '1' stands for the task that should be marked as 'Done'.
 - You will be promted `Marked done successfully`.
 - If you will be trying to mark 'Done' anything twice, or without a proper search, or after some time after the search, the system will promt you that this is not possible: `Requested number for done is out of the menu range`.
- In order to perform the desired action, re-perform search and mark the entry for 'Done' once again.

### Delete (remove) functionality 
- You can delete any entry (both 'Done' and current) any time.
- You need to do the same steps as for "Mark 'Done'" (see above), with the only specifics - instead of `+` introduce `/`, e.g. `/1` to delete the first task in the search.
-  If successfully, you will be prompted: `Marked removed successfully`.
- If search is required to be re-performed, you will be prompted: `Requested number for remove is out of the menu range`.

## Change your name and/or the city
- If you want to change your name, you can do it any time by introducing `change name {newName}`.
- You will be prompted `Your data is successfully changed`.
- You can check that the name is changed correctly by asking tPlanner to re-greet you by typing 'greet'.
- You can change the city the same manner introducing `change city {newCity}.

## Working with the dates
- TPlanner is designed to monitor deadlines. You can introduce a date into your entry and it will be automatically recognised and assigned to your task. E.g. `Finalise familirising with tPlanner by tomorrow`, if introduced on 6th of May, will produce: `Saved: Finalise familirising with tPlanner by tomorrow (category: NoCAT, due: 7 May)`.
- You will be promted on the tasks that are due today the first time you start using tPlanner this day. In response to any of your requests, tPlanner will produce a greeting that would include the following:
```
Due today REMINDER (if any):
```
- All the tasks that are due today will be listed.

### Requesting 'Due today' tasks 
- You can also get the listing of the tasks due today by simply introducing `today`.

### Using absolute data indicators
- tPlanner recognises indication to the month and the particular date. E.g. `10 September`, or `September, 10`.
- If only a month is introduced, e.g. `september`, it will mean assigning to the 1st of September.

### Using week days
- You can introduce any week day from Monday to Sunday to your entry.
- tPlanner will convert the week day to a date using the following formular:
  -- if the introduced weekday is ahead of the today's weekday - the date on the current week will be produced. E.g. if today is Friday, 6 May 2022, and you introduced Sunday, it will produce 8 May (i.e. Sunday this week).
  -- if the introduced weekday is the same as today, or earlier than today, the date the next week will be produced. E.g. if today is Friday, 6 May 2022, and you introduce Thursday, it will produce 12 May (i.e. Thursday the next week).

### Using relative date indicators
- tPlanner can recognise the following relative date indicators: Today, Tomorrow, week, month, quarter, year, and also any season.
- Indication to any relative data would mean the start of the respective interval. For e.g. if you introduce `Review my clothers by summer', the due date will be set as 1 June.

## Shotcuts (in progress)
- instead of `categories` in the search you can simply type `cat`.

## License
For open source projects, say how it is licensed.

## Project status
- First release 1.0 on 9 May 2022.
